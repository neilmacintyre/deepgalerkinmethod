import DGM
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import argparse
import time
import pickle
import os

# Solution parameters (domain on which to solve PDE)
t_low = 0.0   # time lower bound
X_low = 0.0  # Rod lower bound
X_high = 1.0          # Rod upper bound

# terminal time 
T = 1.0

# neural network parameters
num_layers = 5
nodes_per_layer = 10
learning_rate = 0.0001

# Training parameters
sampling_stages  = 100 # number of times to resample new time-space domain points
steps_per_sample = 1000    # number of SGD steps to take before re-sampling

# Sampling parameters
nSim_interior = 1000
nSim_Boundary = 100
nSim_initial = 1000

# multipliers for oversampling i.e. draw X from [X_low - X_oversample, X_high + X_oversample]
X_oversample = 0.0
t_oversample = 0.0

def sampler(nSim_interior, nSim_Boundary, nSim_initial):
    ''' Sample time-space points from the function's domain; points are sampled
        uniformly on the interior of the domain, at the initial/terminal time points
        and along the spatial boundary at different time points. 
    
    Args:
        nSim_interior: number of space points in the interior of the function's domain to sample 
        nSim_initial: number of space points at initial time to sample (initial condition)
    ''' 
    
    # Sampler #1: domain interior    
    # over sampleing
    # t_interior = np.random.uniform(low=t_low - 0.5*(T-t_low), high=T, size=[nSim_interior, 1])
    # X_interior = np.random.uniform(low=X_low - 0.5*(X_high-X_low), high=X_high + 0.5*(X_high-X_low), size=[nSim_interior, 1])
    
    # No over sampling
    t_interior = np.random.uniform(low=t_low, high=T, size=[nSim_interior, 1])
    X_interior = np.random.uniform(low=X_low , high=X_high, size=[nSim_interior, 1])


    # Sampler #2: spatial boundary
    # We only have 2 Boundary Conditions (each end of the rod ie they are both spatial)
    # TODO this brings up the intesting question should we scale this loss 
    # ... we just include all the boundary condtions
    t_boundary = np.random.uniform(low=0, high=T,  size=[nSim_Boundary, 1])
    # X_boundary should be 0 or 1 so we just random sample between 0 and 1 and use the mask >0.5 to create an even distribution
    X_boundary = (np.random.uniform(low=0, high=1,  size=[nSim_Boundary, 1])>0.5).astype(np.float32)
    
    
    # Sampler #3: initial/terminal condition
    t_initial = np.zeros((nSim_initial, 1))
    #X_initial = np.random.uniform(low=0.01 , high=0.99 , size = [nSim_initial, 1])
    X_initial  = np.random.normal(loc=0.5 , scale=0.05 , size = [nSim_initial-50, 1])
    X_initial = np.concatenate((X_initial, np.random.uniform(low=0.01 , high=0.99 , size = [50, 1])))
    
    
    return t_interior, X_interior, t_boundary, X_boundary, t_initial, X_initial

def loss(model, t_interior, X_interior, t_boundary, X_boundary, t_initial, X_initial, ic_number=0):
    ''' Compute total loss for training.
    
    Args:
        model:      DGM model object
        t_interior: sampled time points in the interior of the function's domain
        X_interior: sampled space points in the interior of the function's domain
        t_initial: sampled time points at terminal point (vector of terminal times)
        X_terminal: sampled space points at terminal time
    ''' 
    
    # Loss term #1: PDE
    # compute function value and derivatives at current sampled point
    V = model(t_interior, X_interior)
    V_t = tf.gradients(V, t_interior)[0]
    V_x = tf.gradients(V, X_interior)[0]
    V_xx = tf.gradients(V_x, X_interior)[0]
    # We have that the PDE for heat in a rod is v_t = v_xx -> v_t - v_xx = 0
    diff_V =  V_t - V_xx

    # compute average L2-norm of differential operator
    L1 =   tf.reduce_mean(tf.square(diff_V)) # tf.constant(0, tf.float32)#
    
    # Loss term #2: boundary condition
    # Ends of the rod fixed at temp 0.
    diff_ends = model(t_boundary, X_boundary) - 0 
    L2 = tf.reduce_mean(tf.square(diff_ends)) # tf.constant(0, tf.float32) #
    
    # Loss term #3: initial/terminal condition
    if ic_number == 0:#
        # Triangle start function
        ic_cond = tf.greater(X_initial, 1/2)
        target_initial = tf.where(ic_cond, tf.add(2.0, tf.multiply(-2.0,X_initial)), tf.multiply(2.0,X_initial))
    elif ic_number == 1:
        # Quadratic Starting condition
        target_initial = tf.multiply(tf.multiply(-4.0,X_initial), tf.add(-1.0, X_initial))
    

    fitted_initial = model(t_initial, X_initial)
    
    L3 = tf.reduce_mean( tf.square(fitted_initial - target_initial) )

    return L1, L2, L3



def train_network(label, model=None, ic_number=0, save_network=True, pretrained_model=None):
    global loss

     # initialize DGM model (last input: space dimension = 1)
    if model == None:
        model = DGM.DGMNet(nodes_per_layer, num_layers, 1)


    # tensor placeholders (_tnsr suffix indicates tensors)
    # inputs (time, space domain interior, space domain at initial time)
    t_interior_tnsr = tf.placeholder(tf.float32, [None,1])
    X_interior_tnsr = tf.placeholder(tf.float32, [None,1])
    t_boundary_tnsr = tf.placeholder(tf.float32, [None,1])
    X_boundary_tnsr = tf.placeholder(tf.float32, [None,1])
    t_initial_tnsr = tf.placeholder(tf.float32, [None,1])
    X_initial_tnsr = tf.placeholder(tf.float32, [None,1])

    # loss 
    L1_tnsr, L2_tnsr, L3_tnsr = loss(model, t_interior_tnsr, X_interior_tnsr, t_boundary_tnsr, X_boundary_tnsr, t_initial_tnsr, X_initial_tnsr, ic_number)
    loss_tnsr = L1_tnsr + L2_tnsr + L3_tnsr

    # tensor placeholders (_tnsr suffix indicates tensors)
    # inputs (time, space domain interior, space domain at initial time)
    # t_interior_test_tnsr = tf.placeholder(tf.float32, [None,1])
    # X_interior_test_tnsr = tf.placeholder(tf.float32, [None,1])
    # t_boundary_test_tnsr = tf.placeholder(tf.float32, [None,1])
    # X_boundary_test_tnsr = tf.placeholder(tf.float32, [None,1])
    # t_initial_test_tnsr = tf.placeholder(tf.float32, [None,1])
    # X_initial_test_tnsr = tf.placeholder(tf.float32, [None,1])

    # # loss ic_number

    # set optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss_tnsr)

    # initialize variables
    init_op = tf.global_variables_initializer()

    # open session
    sess = tf.Session()
    if pretrained_model == None:
        sess.run(init_op)
    else:
        saver = tf.train.Saver()
        saver.restore(sess, pretrained_model)
        sess.run(init_op)


    # initialize loss per training
    loss_list = []
    l1_list   = []
    l2_list   = []
    l3_list   = []

    # for each sampling stage
    for i in range(sampling_stages):

        # sample uniformly from the required regions
        t_interior, X_interior, t_boundary, X_boundary, t_initial, X_initial = sampler(nSim_interior, nSim_Boundary, nSim_initial)
    

        # for a given sample, take the required number of SGD steps
        for _ in range(steps_per_sample):
            loss,L1,L2,L3,_ = sess.run([loss_tnsr, L1_tnsr, L2_tnsr, L3_tnsr, optimizer],
                                    feed_dict = 
                                        {t_interior_tnsr:t_interior,
                                        X_interior_tnsr:X_interior,
                                        t_boundary_tnsr:t_boundary,
                                        X_boundary_tnsr:X_boundary,
                                        t_initial_tnsr:t_initial, 
                                        X_initial_tnsr:X_initial})
            loss_list.append(loss)
            l1_list.append(L1)
            l2_list.append(L2)
            l3_list.append(L3)
            
        print(loss, L1, L2, L3, i, label)

    if save_network:
        # Save information on the network
        #path = f'./models/train_{int(time.time())}_{label}/'
        path = f'./models/train_{label}/'
        

        os.makedirs(path)

        config = {
                    'num_layers':num_layers,
                    'nodes_per_layer':nodes_per_layer,
                    'learning_rate':learning_rate,
                    'sampling_stages':sampling_stages,
                    'steps_per_sample':steps_per_sample,
                    'nSim_interior':nSim_interior,
                    'nSim_Boundary':nSim_Boundary,
                    'nSim_initial':nSim_initial,
                    'X_oversample':X_oversample,
                    't_oversample':t_oversample
                }

        loss_curves = {
                        'loss_list':loss_list, 
                        'l1_list':l1_list,
                        'l2_list':l2_list,  
                        'l3_list':l2_list }

        training_record = {
            'config':config,
            'loss_curves':loss_curves
        }

        with open(path + "training_record.pickle", 'wb') as record_file:
            pickle.dump(training_record, record_file)


        saver = tf.train.Saver()
        saver.save(sess, path + "model")


if __name__ == "__main__":

    # Get command arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--ics', default=0, help='ics number')
    parser.add_argument('--label', help='lable for expierment')
    parser.add_argument('--pretrained_model', help='Path to pretrained network to load')
    args = parser.parse_args()

    train_network(args.label, ic_number=int(args.ics), pretrained_model=args.pretrained_model)

    

    


    
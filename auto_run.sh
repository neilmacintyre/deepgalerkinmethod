. ./venv/bin/activate

python HeatInRod.py --ics="0" --label="ICS0_v1"
python HeatInRod.py --ics="1" --label="ICS1_v1"

# Now we will see if the model trains quicker when we start with a pretrained model
python HeatInRod.py --ics="1" --label="ICS1_with_pretraining_ICS0_v1" --pretrained_model="/home/neil/Desktop/DeepGalerkinMethod/models/train_IC0_v1/model"



